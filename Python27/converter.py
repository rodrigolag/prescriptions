# -*- coding: utf-8 -*-
from subprocess import check_output
import sys

path="..\Samples"+"\\"

check_output("python ..\Scripts\scan.py ", shell=True)
check_output("python ..\Scripts\img_cropper.py tmp.bmp", shell=True)
check_output("python ..\OCREngine\print_result.py", shell=True)