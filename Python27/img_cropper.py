# -*- coding: utf-8 -*-
"""
2018/04/06 - bssb
"""
import cv2
import numpy as np
from template_info import *
import sys
from PIL import Image

#Designation of template to be used
template = dict[int(sys.argv[2])][int(sys.argv[3])]

kana = template.get("kana", "")
kanji = template.get("kanji", "")
birth_date = template.get("birth_date", "")
sex = template.get("sex", "")
family = template.get("family", "")
insurance = template.get("insurance", "")
kohi1 = template.get("kohi1", "")
kohi2 = template.get("kohi2", "")
kohi3= template.get("kohi3", "")
doctor_name  = template.get("doctor", "")
#yakuhin = template.get("yakuhin","")

#Yakuhin lines array (empty)
yakuhin_line=[]
#crop image original
img_rgb = cv2.imread(str(sys.argv[1]))
img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
template = cv2.imread('reference2.jpg',0)
w, h = template.shape[::-1]

res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
threshold = 0.8
loc = np.where( res >= threshold)
pt=zip(*loc[::-1])
#for pt in zip(*loc[::-1]):
#    cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)
print(pt)
crop_img = img_rgb[pt[0][1]:2000, pt[0][0]:2000]
cv2.imwrite('cropped.jpg',crop_img)

"""
#Crop image
img_rgb = cv2.imread(str(sys.argv[1]))
img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
template = cv2.imread('reference.jpg',0)
w, h = template.shape[::-1]
totalshift = 0
#crop_img = 0

res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
threshold = 0.6
loc = np.where( res >= threshold)
pt=zip(*loc[::-1])
for pt in zip(*loc[::-1]):
	a=pt[0]
	b=pt[1]
if a > 200:
	shift = 0
	while a > 200:
		shift = shift + 50
		totalshift = totalshift + shift
		a = a - shift

#using translation matrix we shift the image to the left by totalshift
if totalshift > 0:
	M = np.float32([[1,0,-totalshift], [0,1,0]])
	shifted = cv2.warpAffine(img_rgb,M,(img_rgb.shape[1],img_rgb.shape[0])) #shifted images
	crop_img = shifted[b:3000, a:2000]
	cv2.imwrite('cropped.jpg', crop_img)
else:
	crop_img = img_rgb[b:3000, a:2000]
	cv2.imwrite('cropped.jpg', crop_img)
#for pt in zip(*loc[::-1]):
#    cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)
"""

#Loading template images
shohosen_img = crop_img
#print("../Samples/" + str(sys.argv[1]))
kana_img = shohosen_img[kana['y']:kana['y']+kana['h'], kana['x']:kana['x']+kana['w']]
kanji_img = shohosen_img[kanji['y']:kanji['y']+kanji['h'], kanji['x']:kanji['x']+kanji['w']]
sex_img = shohosen_img[birth_date['y']:birth_date['y']+birth_date['h'], birth_date['x']:birth_date['x']+birth_date['w']]
birth_date_img = shohosen_img[sex['y']:sex['y']+sex['h'], sex['x']:sex['x']+sex['w']]
family_img = shohosen_img[family['y']:family['y']+family['h'], family['x']:family['x']+family['w']]
insurance_img = shohosen_img[insurance['y']:insurance['y']+insurance['h'], insurance['x']:insurance['x']+insurance['w']]
kohi1_img = shohosen_img[kohi1['y']:kohi1['y']+kohi1['h'], kohi1['x']:kohi1['x']+kohi1['w']]
kohi2_img = shohosen_img[kohi2['y']:kohi2['y']+kohi2['h'], kohi2['x']:kohi2['x']+kohi2['w']]
kohi3_img = shohosen_img[kohi3['y']:kohi3['y']+kohi3['h'], kohi3['x']:kohi3['x']+kohi3['w']]
doctor_name_img = shohosen_img[doctor_name['y']:doctor_name['y']+doctor_name['h'], doctor_name['x']:doctor_name['x']+doctor_name['w']]

cv2.imwrite('Crops/kana.jpg',kana_img)
cv2.imwrite('Crops/kanji.jpg',kanji_img)
cv2.imwrite('Crops/sex.jpg',sex_img)
cv2.imwrite('Crops/birth_date.jpg',birth_date_img)
cv2.imwrite('Crops/family.jpg',family_img)
cv2.imwrite('Crops/insurance.jpg',insurance_img)

cv2.imwrite('Crops/kohi1.jpg',kohi1_img)
cv2.imwrite('Crops/kohi2.jpg',kohi2_img)
cv2.imwrite('Crops/kohi3.jpg',kohi3_img)
cv2.imwrite('Crops/doctor_name.jpg',doctor_name_img)

"""
for i in range(yakuhin['ln']):
	number_img= shohosen_img[yakuhin['y']+yakuhin['lh']*i+4:yakuhin['y']+yakuhin['lh']*(1+i)+i, yakuhin['number_w']:yakuhin['number_w']+yakuhin['nw']]
	yakuhin_name_img = shohosen_img[yakuhin['y']+yakuhin['lh']*i+4:yakuhin['y']+yakuhin['lh']*(1+i)+i, yakuhin['x']:yakuhin['x']+yakuhin['yakuhin_w']]
	ryo_name_img = shohosen_img[yakuhin['y']+yakuhin['lh']*i+4:yakuhin['y']+yakuhin['lh']*(1+i)+i, yakuhin['x']+yakuhin['yakuhin_w']:yakuhin['x']+yakuhin['yakuhin_w']+yakuhin['ryo_w']]
	kosai_name_img = shohosen_img[yakuhin['y']+yakuhin['lh']*i+4:yakuhin['y']+yakuhin['lh']*(1+i)+i, yakuhin['x']+yakuhin['yakuhin_w']+yakuhin['ryo_w']:yakuhin['x']+yakuhin['yakuhin_w']+yakuhin['ryo_w']+yakuhin['kosai_w']]
	kikan_name_img = shohosen_img[yakuhin['y']+yakuhin['lh']*i+4:yakuhin['y']+yakuhin['lh']*(1+i)+i, yakuhin['x']+yakuhin['yakuhin_w']+yakuhin['ryo_w']+yakuhin['kosai_w']:yakuhin['x']+yakuhin['yakuhin_w']+yakuhin['ryo_w']+yakuhin['kosai_w']+yakuhin['kikan_w']]
	cv2.imwrite('Crops/number'+str(i)+'.jpg',number_img)
	cv2.imwrite('Crops/yakuhin'+str(i)+'.jpg',yakuhin_name_img)
	cv2.imwrite('Crops/ryo'+str(i)+'.jpg',ryo_name_img)
	cv2.imwrite('Crops/kosai'+str(i)+'.jpg',kosai_name_img)
	cv2.imwrite('Crops/kikan'+str(i)+'.jpg',kikan_name_img)
"""


##image_names = ['.../OCREngine/Crops/kana.jpg','../OCREngine/Crops/kanji.jpg','../OCREngine/Crops/sex.jpg','../OCREngine/Crops/birth_date.jpg','../OCREngine/Crops/family.jpg','../OCREngine/Crops/insurance.jpg','../OCREngine/Crops/kohi1.jpg','../OCREngine/Crops/kohi2.jpg','.../OCREngine/Crops/kohi3.jpg', '../OCREngine/Crops/doctor_name.jpg']
##images = []
##max_width = 0 # find the max width of all the images
##total_height = 0 # the total height of the images (vertical stacking)
##
##for name in image_names:
##    # open all images and find their sizes
##    images.append(cv2.imread(name))
##    if images[-1].shape[1] > max_width:
##        max_width = images[-1].shape[1]
##    total_height += images[-1].shape[0]
##
### create a new array with a size large enough to contain all the images
##final_image = np.zeros((total_height,max_width,3),dtype=np.uint8)
##
##current_y = 0 # keep track of where your current image was last placed in the y coordinate
##for image in images:
##    # add an image to the final array and increment the y coordinate
##    final_image[current_y:image.shape[0]+current_y,:image.shape[1],:] = image
##    current_y += image.shape[0]
##
##cv2.imwrite('crop.jpg',final_image)
