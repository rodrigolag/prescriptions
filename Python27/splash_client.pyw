# -*- coding: utf-8 -*-
import wx
from subprocess import check_output

class SplashScreen(wx.Frame):
    def __init__(self, parent, ID=-1, title="SplashScreen",
                 style=wx.SIMPLE_BORDER | wx.STAY_ON_TOP ,
                 duration=1500, bitmapfile="bitmaps/splashscreen.bmp",
                 callback=None):
        '''
        parent, ID, title, style -- see wx.Frame
        duration -- milliseconds to display the splash screen
        bitmapfile -- absolute or relative pathname to image file
        callback -- if specified, is called when timer completes, callback is
                    responsible for closing the splash screen
        '''
        ### Loading bitmap
        self.bitmap = bmp = wx.Image(bitmapfile, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        ### Determine size of bitmap to size window...
        size = (bmp.GetWidth(), bmp.GetHeight())
        # size of screen
        width = 420
        height = 320
        #pos = ((width - size[0]) / 2, (height - size[1]) / 2)
        pos = (600, 350)
        # check for overflow...
        if pos[0] < 0:
            size = (600, size[1])
        if pos[1] < 0:
            size = (size[0], 400)
        wx.Frame.__init__(self, parent, ID, title, pos, size, style)
        self.Bind(wx.EVT_LEFT_DOWN, self.OnMouseClick)
        self.Bind(wx.EVT_CLOSE, self.OnCloseWindow)
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_ERASE_BACKGROUND, self.OnEraseBG)
        self.Show(True)
        #print duration
        class SplashTimer(wx.Timer):
            def __init__(self, targetFunction):
                self.Notify = targetFunction
                wx.Timer.__init__(self)
        if callback is None:
            callback = self.OnSplashExitDefault
        self.timer = SplashTimer(callback)
        # self.Bind(wx.EVT_TIMER, self.OnSplashExitDefault, self.timer)
        self.timer.Start(duration, False) # one-shot only
    def OnPaint(self, event):
        dc = wx.PaintDC(self)
        dc.DrawBitmap(self.bitmap, 0, 0, False)
    def OnEraseBG(self, event):
        pass
    def OnSplashExitDefault(self, event=None):
        #print "close my'self by time"
        self.Close(True)
    def OnCloseWindow(self, event=None):
        self.Show(False)
        self.timer.Stop()
        del self.timer
        self.Destroy()
    def OnMouseClick(self, event):
        self.timer.Notify()
class DemoApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        self.splash = SplashScreen(None, duration=3000, bitmapfile="..\Images\logo.bmp", callback=self.OnSplashExit)
        self.splash.Show(True)
        self.SetTopWindow(self.splash)
        return True
    def OnSplashExit(self, event=None):
        #print "Yay! Application callback worked!"
        self.splash.Close(True)
        del self.splash
        check_output("python.exe client.py", shell=True)
        ### Build working windows here...
#----------------------------------------------------------------------
if __name__ == "__main__":
    def test(sceneGraph=None):
        app = DemoApp(0)
        app.MainLoop()
    test()
