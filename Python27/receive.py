# -*- coding: utf-8 -*-
"""
2018/04/06 - bssb
"""
from pylsl import StreamInfo, StreamOutlet, StreamInlet, resolve_stream
import time
import sys  

dispenser_number = int(sys.argv[1])
fill_flag=0

#Create Stream Outlet to send information to other computers thtough LSL
info = StreamInfo('Mediconfia', 'Confirmation', 1, 0, 'string', 'master_thread')
outlet = StreamOutlet(info)


# first resolve an EEG stream on the lab network
print("looking for an Macro stream...")
streams = resolve_stream('type', 'Macro_Output')

# create a new inlet to read from the stream
inlet = StreamInlet(streams[0])

commands = open("..\Output\commands.txt","w")

while True:

	while True:
	    # get a new sample (you can also omit the timestamp part if you're not
	    # interested in it)
	    sample, timestamp = inlet.pull_sample()

	    x = sample[0].rstrip()
	    y= sample[1].rstrip()
	    #print(sample[0].rstrip(), sample[1].rstrip())

	    if x!="START" and fill_flag==1 and x!="END":
		    #print("Flag")
		    commands.write(x.encode('utf8') + "," + y.encode('utf8') + "\n")
	    	#outlet.push_sample(["OK"])

	    if x=="START" and int(y)==dispenser_number:
			print("START FILLING ROUTINE")
			fill_flag=1

	    if x=="END" and int(y)==dispenser_number:
	    	fill_flag=0
	    	commands.close()
	    	#EXECUTE MACRO
	    	time.sleep(3) #simulation of macro execution
	    	print("FINISHED FILLING ROUTINE")
	    	commands = open("..\Output\commands.txt","w")

	    	outlet.push_sample(["OK"])

	    	break


print("finish")