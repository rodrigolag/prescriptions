# -*- coding: utf-8 -*-
"""
2018/04/06 - Mediconfia Shohosen Master V0.17
Author: BSSB
Scan Module connected to TWAIN
"""

import wx
import wx.grid as gridlib
import time
from simple_base import TwainBase
from subprocess import check_output
import sys
import traceback, sys
from pharmacy_lists import *
from template_info import *
from PIL import Image
import jaconv
from difflib import SequenceMatcher
import csv
import numpy as np
import re
import warnings

#IGNORE WARNINGS
warnings.filterwarnings("ignore")

from pylsl import StreamInfo, StreamOutlet, StreamInlet, resolve_stream

reload(sys)
sys.setdefaultencoding('utf8')

##################### LSL CONNECTION ##################################

#Create Stream Outlet to send information to other computers thtough LSL
info = StreamInfo('Mediconfia', 'Macro_Output', 2, 0, 'string', 'master_thread')
outlet = StreamOutlet(info)

#Confirmation stream for the protocol
#print("looking for a Confirmation stream...")
streams = resolve_stream('type', 'Confirmation')
inlet = StreamInlet(streams[0])

########################################################################

ID_EXIT=102
ID_OPEN_SCANNER=103
ID_ACQUIRE_NATIVELY=104
ID_SCROLLEDWINDOW1=105
ID_BMPIMAGE=106
ID_ACQUIRE_BY_FILE=107
ID_TIMER=108

# You can either Poll the TWAIN source, or process the scanned image in an
# event callback. The event callback has not been fully tested using GTK.
# Specifically this does not work with Tkinter.
USE_CALLBACK=True

global txt_kana
global txt_kanji
global txt_insurance
global txt_kohi1
global txt_kohi2
global txt_kohi3
global txt_birth_date
global era              #this
global txt_sex
global txt_sex_male     #this
global txt_sex_female   #this
global txt_family
global txt_doctor_name
global shoho
global panel
global template
global template_idx
global pharmacy_idx
global pharmacy_customer_idx
global dispenser_idx

#Default for template idx and pharmacy idx
template_idx=0
pharmacy_idx=0
pharmacy_customer_idx=0
dispenser_idx=1

#output data paths
global data_path
global yakuhin_code_path
global yakuhin_name_path
global yakuhin_final
global ryo_path
global kosai_path
global kikan_path
global yakuhin_paths

data_path="..\Output\data_modified.txt"
yakuhin_code_path="..\Output\yakuhin_code.txt"
yakuhin_name_path="..\Output\yakuhin_final.txt"
ryo_path="..\Output"+"\\"+"ryo_modified.txt"
kosai_path="..\Output\kosai_modified.txt"
kikan_path="..\Output\kikan_modified.txt"

yakuhin_paths = [yakuhin_code_path, yakuhin_name_path, ryo_path, kosai_path, kikan_path]

yakuhin_update="..\Output\yakuhin_update.txt"

global yakuhin_master_file_path

yakuhin_master_file_path = "..\Log\M_Drug.log"

global dispenser_list

dispenser_list = ['分包機１','分包機２','分包機３']

#Template update functions
def convert(word):
    w=unicode(str(word), "utf-8")
    return jaconv.z2h(w)

#Similarity function
def similar(a, b):
    return SequenceMatcher(None, unicode(a), b).ratio()


def createTMP_DB(path):
    #Open file
    with open(path, 'r') as in_file:
        stripped = (line.strip() for line in in_file)
        lines = (line.split(",") for line in stripped if line)
        with open('log.csv', 'w') as out_file:
            writer = csv.writer(out_file)
            writer.writerows(lines)

    #Clean Log File
    with open('log.csv', 'r') as f:
        lines = f.readlines()

        # remove spaces and not wanted marks
        lines = [line.replace('[', '') for line in lines]
        lines = [line.replace(']', '') for line in lines]
        lines = [line.replace('\n', '') for line in lines]

    with open('log.csv', 'w') as f:
        f.writelines(lines)
    f.close()

    #Create temporary database
    with open("yakuhin.csv","w") as f:
        with open('log.csv', 'rU') as csvfile:
            lexics = csv.reader(csvfile, delimiter=',')
            for row in lexics:
                f.write(str(row[8])+","+str(row[9])+"\n")
    f.close()


class MainFrame(wx.Frame, TwainBase):
    """wxPython implementation of the simple demonstration"""
    def __init__(self, parent, id, title):
        global txt_kana
        global txt_kanji
        global txt_insurance
        global txt_kohi1
        global txt_kohi2
        global txt_kohi3
        global txt_birth_date
        global txt_sex
        global txt_family
        global txt_doctor_name
        global shoho
        global panel
        global dispenser_list

        offset_x=120
        offset_y=100

        wx.Frame.__init__(self, parent, id, title,
        wx.DefaultPosition, wx.Size(1280,1024))

        #Company icon and logo
        ico = wx.Icon('../Images/icon.ico', wx.BITMAP_TYPE_ICO)
        self.SetIcon(ico)
        font0 = wx.Font(24, wx.DECORATIVE, wx.ITALIC, wx.NORMAL)
        font0 = wx.Font(20, wx.DECORATIVE, wx.ITALIC, wx.NORMAL)
        font1 = wx.Font(14, wx.DECORATIVE, wx.ITALIC, wx.NORMAL)
        font2 = wx.Font(10, wx.DECORATIVE, wx.ITALIC, wx.NORMAL)
        font3 = wx.Font(12, wx.DECORATIVE, wx.ITALIC, wx.NORMAL)

        #self.Maximize(True)
        self.CreateStatusBar()
        menu = wx.Menu()
        menu.Append(ID_OPEN_SCANNER, "ソースの選択", "スキャナに接続する")
        menu.Append(ID_ACQUIRE_NATIVELY, "スキャン", "ネ処方箋画像を取得する")
        #menu.Append(ID_ACQUIRE_BY_FILE, "Acquire By &File", "Acquire an Image using File Transfer Interface")
        menu.AppendSeparator()
        menu.Append(ID_EXIT, "閉じる", "プログラムを終了する")
        menuBar = wx.MenuBar()
        menuBar.Append(menu, "ファイル")
        self.SetMenuBar(menuBar)

        wx.EVT_MENU(self, ID_EXIT, self.MnuQuit)
        wx.EVT_MENU(self, ID_OPEN_SCANNER, self.MnuOpenScanner)
        wx.EVT_MENU(self, ID_ACQUIRE_NATIVELY, self.MnuAcquireNatively)
        wx.EVT_MENU(self, ID_ACQUIRE_BY_FILE, self.MnuAcquireByFile)
        wx.EVT_CLOSE(self, self.OnClose)

        #Panel positioning for all information
        panel = wx.Panel(self, -1)

        bmp1 = wx.Image('../Images/logo.bmp', wx.BITMAP_TYPE_ANY).ConvertToBitmap()
        self.bitmap1 = wx.StaticBitmap( panel, -1, bmp1, (50, 20))


        #Prescription image label
        picture_label = wx.StaticText(panel, pos=(20, 110), size=(100, 100), label="処方箋選択：")
        picture_label.SetFont(font0)

        self.scrolledWindow1 = wx.ScrolledWindow(panel,wx.ID_ANY,pos=(10, 150), size=(600, 650))
        self.bmpImage = wx.StaticBitmap(bitmap = wx.NullBitmap, id = ID_BMPIMAGE, name = 'bmpImage', parent = self.scrolledWindow1, pos = wx.Point(0,0), style = 0)

        #Choice boxes added
        self.pharmacy_choice = wx.Choice(panel, -1, (200, 115), choices=pharmacy_list)
        self.pharmacy_choice.Bind(wx.EVT_CHOICE, self.OnPharmacyChoice)
        self.pharmacy_choice.SetSelection(0)

        self.pharmacy_customer_choice = wx.Choice(panel, -1, (350, 115), choices=pharmacy_customers[pharmacy_idx])
        self.pharmacy_customer_choice.Bind(wx.EVT_CHOICE, self.OnPharmacyCustomerChoice)
        self.pharmacy_customer_choice.SetSelection(0)

        picture_label = wx.StaticText(panel, pos=(730, 830), size=(50, 20), label="分包機選択：")
        picture_label.SetFont(font1)

        self.dispenser_choice = wx.Choice(panel, -1, (750, 870), choices=dispenser_list)
        self.dispenser_choice.Bind(wx.EVT_CHOICE, self.OnDispenserChoice)
        self.dispenser_choice.SetSelection(0)

        #Patient info label
        patient_label = wx.StaticText(panel, pos=(620, 10), size=(250, 50), label="患者情報")
        patient_label.SetFont(font0)

        #Source selection button
        self.btn_source = wx.Button(panel, label="ソース選択",pos=(145, 820), size=(90, 90))
        self.btn_source.SetFont(font2)
        self.btn_source.Bind(wx.EVT_BUTTON,self.MnuOpenScanner)

        #Scan prescription button
        self.btn_scan = wx.Button(panel, label="処方箋\nスキャン",pos=(295, 820), size=(90, 90))
        self.btn_scan.SetFont(font2)
        self.btn_scan.Bind(wx.EVT_BUTTON,self.MnuAcquireNatively)

        #Scan prescription button
        self.btn_fill = wx.Button(panel, label="データ取得",pos=(445, 820), size=(90, 90))
        self.btn_fill.SetFont(font2)
        self.btn_fill.Bind(wx.EVT_BUTTON,self.MnuFill)

        #Automatic update
        self.btn_fill = wx.Button(panel, label="更新",pos=(900, 820), size=(100, 90))
        self.btn_fill.SetFont(font2)
        self.btn_fill.Bind(wx.EVT_BUTTON,self.DBUpdate)                                         #original y position - 820 (buttons)

        #Automatic fill button
        self.btn_fill = wx.Button(panel, label="自動Do入力",pos=(1050, 500), size=(100, 90))     #modified the y position
        self.btn_fill.SetFont(font2)
        self.btn_fill.Bind(wx.EVT_BUTTON,self.MnuFill)

        #Patient
        patient_label = wx.StaticText(panel, pos=(620, 90), size=(30, 50), label="患\n者")
        patient_label.SetFont(font0)

        patient_labels = wx.StaticText(panel, pos=(620, 490), size=(30, 50), label="処\n\n\n\n\n\n\n方")
        patient_labels.SetFont(font0)

        patient_label1 = wx.StaticText(panel, pos=(670, 110), size=(40, 30), label="氏名")
        patient_label1.SetFont(font1)

        txt_kana = wx.TextCtrl(panel, -1, 'かな', pos=(720, 80), size=(200, 30))
        txt_kana.SetFont(font1)

        txt_kanji = wx.TextCtrl(panel, -1, '漢字', pos=(720, 120), size=(200, 30))
        txt_kanji.SetFont(font1)

        patient_label9 = wx.StaticText(panel, pos=(880+offset_x, 40), size=(50, 30), label="保険医氏名")
        patient_label9.SetFont(font1)

        txt_doctor_name = wx.TextCtrl(panel, -1, '氏名', pos=(980+offset_x, 35), size=(125, 30))
        txt_doctor_name.SetFont(font1)

        patient_label2 = wx.StaticText(panel, pos=(840+offset_x, 85), size=(50, 30), label="保険保険者番号")
        patient_label2.SetFont(font1)

        txt_insurance = wx.TextCtrl(panel, -1, '番号', pos=(980+offset_x, 80), size=(125, 30))
        txt_insurance.SetFont(font1)

        patient_label3 = wx.StaticText(panel, pos=(805+offset_x, 140), size=(50, 30), label="公費負担者番号「上」")
        patient_label3.SetFont(font1)

        txt_kohi1 = wx.TextCtrl(panel, -1, '番号', pos=(980+offset_x, 135), size=(125, 30))
        txt_kohi1.SetFont(font1)

        patient_label4 = wx.StaticText(panel, pos=(860+offset_x, 185), size=(50, 30), label="公費負担医療\nの受給者番号")
        patient_label4.SetFont(font1)

        txt_kohi2 = wx.TextCtrl(panel, -1, '番号', pos=(980+offset_x, 195), size=(125, 30))
        txt_kohi2.SetFont(font1)

        patient_label5 = wx.StaticText(panel, pos=(805+offset_x, 250), size=(50, 30), label="公費負担者番号「下」")
        patient_label5.SetFont(font1)

        txt_kohi3 = wx.TextCtrl(panel, -1, '番号', pos=(980+offset_x, 245), size=(125, 30))
        txt_kohi3.SetFont(font1)

        patient_label6 = wx.StaticText(panel, pos=(620, 180), size=(50, 30), label="生年月日")
        patient_label6.SetFont(font1)

        txt_birth_date = wx.TextCtrl(panel, -1, '生年月日', pos=(720, 180), size=(200, 30))
        txt_birth_date.SetFont(font1)

        patient_label7 = wx.StaticText(panel, pos=(620, 250), size=(50, 30), label="性別")
        patient_label7.SetFont(font1)

        txt_sex = wx.TextCtrl(panel, -1, '性', pos=(670, 245), size=(50, 30))
        txt_sex.SetFont(font1)

        patient_label8 = wx.StaticText(panel, pos=(730, 250), size=(50, 30), label="区分")
        patient_label8.SetFont(font1)

        txt_family = wx.TextCtrl(panel, -1, '区分', pos=(780, 245), size=(140, 30))
        txt_family.SetFont(font1)

        #Grid values for prescriptions
        shoho = gridlib.Grid(panel,pos=(680, 350),size=(520, 450), style=0)
        shoho.CreateGrid(100, 5)
        shoho.HideColLabels()
        shoho.HideRowLabels()
        shoho.SetColSize(0, 100)
        shoho.SetColSize(1, 250)
        shoho.SetColSize(2, 50)
        shoho.SetColSize(3, 50)
        shoho.SetColSize(4, 50)

        yakuhin_label = wx.StaticText(panel, pos=(690, 330), size=(50, 20), label="コード")
        yakuhin_label.SetFont(font3)

        yakuhin_label1 = wx.StaticText(panel, pos=(780, 330), size=(50, 20), label="行")
        yakuhin_label1.SetFont(font3)

        yakuhin_label3 = wx.StaticText(panel, pos=(1045, 330), size=(50, 20), label="量")
        yakuhin_label3.SetFont(font3)

        yakuhin_label4 = wx.StaticText(panel, pos=(1085, 330), size=(50, 20), label="形態")
        yakuhin_label4.SetFont(font3)

        yakuhin_label5 = wx.StaticText(panel, pos=(1135, 330), size=(50, 20), label="期間")
        yakuhin_label5.SetFont(font3)

        # Print out the exception - requires that you run from the command prompt
        sys.excepthook = traceback.print_exception

        # Initialise the Twain Base Class
        self.Initialise()

        # Polling based example
        if not USE_CALLBACK:
            wx.EVT_TIMER(self, ID_TIMER, self.onIdleTimer)
            self.timer=wx.Timer(self, ID_TIMER)
            self.timer.Start(250)

    def MnuQuit(self, event):
        self.Close(1)

    def OnClose(self, event):
        # Terminate the Twain Base Class
        self.Terminate()
        self.Destroy()

    def MnuOpenScanner(self, event):
        self.OpenScanner(self.GetHandle(), ProductName="処方箋マスター", UseCallback=USE_CALLBACK)

    def MnuAcquireNatively(self, event):
        #Repaint Image Box
        global template

        template = dict[pharmacy_idx][pharmacy_customer_idx]

        result = self.AcquireNatively()

        #angle = template.get("rotate", "")
        img=Image.open("torotate.bmp")
        img=img.rotate(template.get("rotate", "")['value'],expand=True)
        img.save("rotated.bmp")

        #Regenerating images
        self.scrolledWindow1.Destroy()

        self.scrolledWindow1 = wx.ScrolledWindow(panel,wx.ID_ANY,pos=(10, 150), size=(600, 650))
        self.bmpImage = wx.StaticBitmap(bitmap = wx.NullBitmap, id = ID_BMPIMAGE, name = 'bmpImage', parent = self.scrolledWindow1, pos = wx.Point(0,0), style = 0)
        self.DisplayImage("rotated.bmp")

        return result

    def MnuAcquireByFile(self, event):
        return self.AcquireByFile()

    def MnuFill(self, event):
        global txt_kana
        global txt_kanji
        global txt_insurance
        global txt_kohi1
        global txt_kohi2
        global txt_kohi3
        global txt_birth_date
        global era              #this
        global txt_sex
        global txt_sex_male     #this
        global txt_sex_female   #this
        global txt_family
        global txt_doctor_name
        global shoho
        global template_idx

        global data_path
        global yakuhin_code_path
        global yakuhin_final
        global ryo_path
        global kosai_path
        global kikan_path
        global yakuhin_paths

        #Perform OCR Recognition and conversion
        check_output("python img_cropper.py OPENcv_TWAIN_Adaptative2.jpg " + str(pharmacy_idx)+ " " + str(pharmacy_customer_idx), shell=True)
        check_output("python print_result.py "+  str(pharmacy_idx) + " " + str(pharmacy_customer_idx), shell=True)

        with open (data_path, "r") as myfile:
            data=myfile.readlines()


        txt_kana.SetLabel(data[0])
        txt_kanji.SetLabel(data[1])
        txt_insurance.SetLabel(data[6])
        txt_kohi1.SetLabel(data[7])
        txt_kohi2.SetLabel(data[8])
        #txt_kohi3.SetLabel(data[9])
        txt_birth_date.SetLabel(data[2])
        txt_sex.SetLabel(data[3])
        txt_family.SetLabel(data[4])
        txt_doctor_name.SetLabel(data[5])

        """for i in range(len(yakuhin_paths)):
            counter=0
            with open(yakuhin_paths[i],"rb") as f:
                for line in f:
                    shoho.SetCellValue(counter,i, line)
                    counter+=1
        """
        wx.MessageBox('データ取得成功！', '完了', wx.OK | wx.ICON_INFORMATION)

    def AutomaticInput(self, event):
        global shoho
        global dispenser_idx
        count = len(open("../Output/yakuhin_final.txt").readlines(  ))

        #print("START "+ str(dispenser_idx))

        #Start LSL Transmission
        code_stream = ["START", str(dispenser_idx)]


        ####LSL STREAM ######
        outlet.push_sample(code_stream)

        #sample, timestamp = inlet.pull_sample()
        ####################

        code_list=[]
        value_list=[]

        code_file = open("..\Output\code.txt","w")
        value_file = open("..\Output\quantity.txt","w")
        for k in range(count):
            code= shoho.GetCellValue(k,0)
            ryo = shoho.GetCellValue(k,2)
            kikan = shoho.GetCellValue(k,4)

            #print(code)

            if code!='\r\n' and ryo=='\r\n' and kikan=='\r\n':

                code_list.append(code)

                code_file.write(code)

            if code=='\r\n' and ryo!='\r\n' and kikan=='\r\n':

                value_list.append(ryo.encode('ascii', 'ignore'))
                value_file.write(ryo.encode('ascii', 'ignore'))

            if code!='\r\n' and ryo=='\r\n' and kikan!='\r\n':

                code_list.append(code)
                value_list.append(kikan.encode('ascii', 'ignore'))

                code_file.write(code)
                value_file.write(kikan.encode('ascii', 'ignore'))

            if code!='\r\n' and ryo!='\r\n' and kikan=='\r\n':

                code_list.append(code)
                value_list.append(ryo.encode('ascii', 'ignore'))

                code_file.write(code)
                value_file.write(ryo.encode('ascii', 'ignore'))

        code_file.close()
        value_file.close()

        code_length = len(open("..\Output\code.txt").readlines(  ))

        code_value=[]
        value=[]
        code_stream=[code_value,value]

        with open("..\Output\code.txt", 'r') as infile:
            for line in infile:
                code_value.append(line.rstrip())

        with open("..\Output\quantity.txt", 'r') as infile:
            for line in infile:
                value.append(line.rstrip())

        for m in range(code_length):
            #final.write(code_value[m] + ":" + value[m])

            #code_file.write(code + "," + value_list)
            #print(code_value[m] + ":" + value[m])
            code_stream = [code_value[m], value[m]]
            #now send it and wait for a bit

            ##############LSL STREAM ############
            outlet.push_sample(code_stream)
            time.sleep(0.01)

            #sample, timestamp = inlet.pull_sample()
            ######################################


        code_stream = ["END", str(dispenser_idx)]
        outlet.push_sample(code_stream)
        time.sleep(0.1)

        #Wait for system confirmation
        sample, timestamp = inlet.pull_sample()

        wx.MessageBox('自動Do入力成功！', '完了', wx.OK | wx.ICON_INFORMATION)

    def DBUpdate(self, event):
        global shoho
        global yakuhin_paths
        global yakuhin_code_path
        global yakuhin_name_path
        global yakuhin_master_file_path

        count = len(open("../Output/yakuhin_final.txt").readlines(  ))
        row_value=[]
        yakuhin_code_dictionary=[]
        yakuhin_dictionary=[]
        similarity =[]

        #createTMP_DB(yakuhin_master_file_path)

        db_yakuhin = "yakuhin_code.csv"
        db_iho = "iho_code.csv"

        db_files = [db_yakuhin, db_iho]
        yakuhin_code = open("..\Output\yakuhin_code.txt","w")
        yakuhin_final = open("..\Output\yakuhin_final.txt","w")

        #Update other values
        ryo_final = open("..\Output"+"\\"+"ryo_final.txt","w")
        kosai_final = open("..\Output\kosai_final.txt","w")
        kikan_final = open("..\Output\kikan_final.txt","w")

        file_selector = 0

        for k in range(count):
            yakuhin = shoho.GetCellValue(k,1)
            #print(similar("﻿以下余白", convert(yakuhin)))
            ryo = str(shoho.GetCellValue(k,2))
            kosai = str(shoho.GetCellValue(k,3))

            ryo_final.write(ryo)
            kosai_final.write(kosai)

            kikan = str(shoho.GetCellValue(k,4))
            kikan_final.write(kikan)
            if len(kikan)<12:
                file_selector=0
            else:
                file_selector=1
            #print(len(kikan))
            index_yakuhin = []
            similarity_value_yakuhin =[]
            dictionary_value_yakuhin =[]

            with open(db_files[file_selector], 'rb') as csvfile:
                lexic = csv.reader(csvfile, delimiter=',')
                for row in lexic:

                    similarity.append(similar(row[1], convert(yakuhin)))
                    yakuhin_code_dictionary.append(row[0])
                    yakuhin_dictionary.append(row[1])

            index_yakuhin.append(yakuhin_code_dictionary[similarity.index(np.max(similarity))]+"\n")
            similarity_value_yakuhin.append(similarity[similarity.index(np.max(similarity))+1])
            dictionary_value_yakuhin.append(yakuhin_dictionary[similarity.index(np.max(similarity))]+"\n")
            similarity=[]
            yakuhin_code_dictionary=[]
            yakuhin_dictionary=[]
            similarity_final = np.max(similarity_value_yakuhin)
            if file_selector==0:
                if similarity_final>0.1:
                    yakuhin_code.write(index_yakuhin[similarity_value_yakuhin.index(similarity_final)])
                    yakuhin_final.write(dictionary_value_yakuhin[similarity_value_yakuhin.index(similarity_final)])
                else:
                    yakuhin_code.write("\n")
                    yakuhin_final.write("\n")
            else:
                yakuhin_code.write(index_yakuhin[similarity_value_yakuhin.index(similarity_final)])
                yakuhin_final.write(dictionary_value_yakuhin[similarity_value_yakuhin.index(similarity_final)])


        yakuhin_code.close()
        yakuhin_final.close()
        ryo_final.close()
        kosai_final.close()
        kikan_final.close()

        for i in range(2):
            counter=0
            with open(yakuhin_paths[i],"rb") as f:
                for line in f:
                    shoho.SetCellValue(counter,i, line)
                    counter+=1
        wx.MessageBox('更新成功！', '完了', wx.OK | wx.ICON_INFORMATION)

    def OnDispenserChoice(self,event):
        global dispenser_list
        global dispenser_idx
        global panel

        dispenser_idx=self.dispenser_choice.GetSelection()+1

        #Repaing pharmacy choices
        #print("You selected "+ str(dispenser_idx)+" from Choice")

        self.dispenser_choice.Destroy()
        self.dispenser_choice = wx.Choice(panel, -1, (750, 870), choices=dispenser_list)
        self.dispenser_choice.Bind(wx.EVT_CHOICE, self.OnDispenserChoice)
        self.dispenser_choice.SetSelection(dispenser_idx-1)

    def OnPharmacyChoice(self,event):
        global pharmacy_idx
        global panel
        #print("You selected "+ str(self.pharmacy_choice.GetSelection())+" from Choice")

        pharmacy_idx=self.pharmacy_choice.GetSelection()

        #Repaing pharmacy choices
        self.pharmacy_customer_choice.Destroy()
        self.pharmacy_customer_choice = wx.Choice(panel, -1, (350, 115), choices=pharmacy_customers[pharmacy_idx])
        self.pharmacy_customer_choice.Bind(wx.EVT_CHOICE, self.OnPharmacyCustomerChoice)

    def OnPharmacyCustomerChoice(self,event):
        global pharmacy_customer_idx
        #print("You selected "+ str(pharmacy_idx*100+self.pharmacy_customer_choice.GetSelection())+" from Choice")
        pharmacy_customer_idx=self.pharmacy_customer_choice.GetSelection()
        template_idx = (100*pharmacy_idx) + pharmacy_customer_idx

    def DisplayImage(self, ImageFileName):
        bmp = wx.Image(ImageFileName, wx.BITMAP_TYPE_BMP).ConvertToBitmap()
        image = wx.ImageFromBitmap(bmp)
        image = image.Scale(10, 10, wx.IMAGE_QUALITY_HIGH)
        self.bmpImage.SetBitmap(bmp)
        self.scrolledWindow1.maxWidth = bmp.GetWidth()
        self.scrolledWindow1.maxHeight = bmp.GetHeight()
        self.scrolledWindow1.SetScrollbars(40, 40, bmp.GetWidth()/40, bmp.GetHeight()/40)
        self.bmpImage.Refresh()

    def LogMessage(self, message):
        # Set the title on the main window - used for tracing
        self.SetTitle(message)

    def onIdleTimer(self, event=None):
        """This is a polling mechanism. Get the image without relying on the callback."""
        self.PollForImage()


class SimpleApp(wx.App):
    def OnInit(self):
        frame = MainFrame(None, -1, "処方箋マスター　V.0.1")
        frame.Show(True)
        self.SetTopWindow(frame)
        return 1

SimpleApp(0).MainLoop()

#Commenting out information for template matching and selection
