# -*- coding: utf-8 -*-
import wx  
from subprocess import check_output
from pylsl import StreamInfo, StreamOutlet, StreamInlet, resolve_stream
import time
import sys  
import threading

global dispenser_list
dispenser_list = ['分包機１','分包機２','分包機３']

global device_idx
device_idx = 1


fill_flag=0

#Create Stream Outlet to send information to other computers thtough LSL
info = StreamInfo('Mediconfia', 'Confirmation', 1, 0, 'string', 'master_thread')
outlet = StreamOutlet(info)


# first resolve an EEG stream on the lab network
print("looking for an Macro stream...")
streams = resolve_stream('type', 'Macro_Output')

# create a new inlet to read from the stream
inlet = StreamInlet(streams[0])

global commands
commands = open("..\Output\commands.txt","w")

def Receive():
  global device_idx
  global commands
  dispenser_number=device_idx
  while True:
      while True:
        # get a new sample (you can also omit the timestamp part if you're not
        # interested in it)
        sample, timestamp = inlet.pull_sample()

        x = sample[0].rstrip()
        y= sample[1].rstrip()
        #print(sample[0].rstrip(), sample[1].rstrip())

        if x!="START" and fill_flag==1 and x!="END":
            #print("Flag")
            commands.write(x.encode('utf8') + "," + y.encode('utf8') + "\n")
            #outlet.push_sample(["OK"])

        if x=="START" and int(y)==dispenser_number:
            print("START FILLING ROUTINE")
            fill_flag=1

        if x=="END" and int(y)==dispenser_number:
            fill_flag=0
            commands.close()
            #EXECUTE MACRO
            time.sleep(3) #simulation of macro execution
            print("FINISHED FILLING ROUTINE")
            commands = open("..\Output\commands.txt","w")

            outlet.push_sample(["OK"])

            break

class Mywin(wx.Frame): 
   def __init__(self, parent, title): 
      super(Mywin, self).__init__(parent, title = title,size = (300,300)) 
      
      ico = wx.Icon('../Images/icon.ico', wx.BITMAP_TYPE_ICO)
      self.SetIcon(ico)        
      
      panel = wx.Panel(self) 
      box = wx.BoxSizer(wx.VERTICAL) 
      self.label = wx.StaticText(panel,label = u' 分包機を選択して下さい' ,style = wx.ALIGN_CENTRE) 
      box.Add(self.label, 0 , wx.EXPAND |wx.ALIGN_CENTER_HORIZONTAL |wx.ALL, 20) 

      chlbl = wx.StaticText(panel,label = "分包機選択：",style = wx.ALIGN_CENTRE) 
        
      box.Add(chlbl,0,wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL|wx.ALL,5) 
      self.choice = wx.Choice(panel,choices = dispenser_list) 
      self.choice.SetSelection(0)
      box.Add(self.choice,1,wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL|wx.ALL,5) 

      self.btn_source = wx.Button(panel, label="スタート",pos=(145, 820), size=(90, 90))
      
      box.Add(self.btn_source,2,wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL|wx.ALL,5) 
         
      box.AddStretchSpacer() 

      self.choice.Bind(wx.EVT_CHOICE, self.OnChoice)
      self.btn_source.Bind(wx.EVT_BUTTON,self.Start_Client) 
        
      panel.SetSizer(box) 
      self.Centre() 
      self.Show() 
                 
   def OnChoice(self,event): 
      global device_idx
      self.label.SetLabel(self.choice.GetString
         (self.choice.GetSelection())+u' を選択しました') 
      device_idx=self.choice.GetSelection()+1


   def Start_Client(self,event): 

      t = threading.Thread(target=Receive)
      t.start()
      self.label.SetLabel(u' スタート成功でした。') 

                             
app = wx.App() 
Mywin(None,  '機械選択') 
app.MainLoop()
