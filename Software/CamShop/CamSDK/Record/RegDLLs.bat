:: Administrator privilege is required!
@echo off
regsvr32 /s "%~dp0ScnCap.ax"
regsvr32 /s "%~dp0AudCap.ax"
regsvr32 /s "%~dp0AacEnc.ax"
regsvr32 /s "%~dp0Mp4Mux.ax"
regsvr32 /s "%~dp0FlvMux.ax"
regsvr32 /s "%~dp0Stream.ax"
regsvr32 /s "%~dp0wavdest.ax"
regsvr32 /s "%~dp0lame.ax"
regsvr32 /s "%~dp0AvcEnc.ax"
@echo on