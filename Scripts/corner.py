# -*- coding: utf-8 -*-
"""
2018/04/06 - bssb
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt

img_rgb = cv2.imread('line.bmp')
img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
template = cv2.imread('corner.bmp',0)
w, h = template.shape[::-1]

res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
threshold = 0.8
loc = np.where( res >= threshold)
pt=zip(*loc[::-1])
#for pt in zip(*loc[::-1]):
#    cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)
print(pt[0][0])
crop_img = img_rgb[pt[0][1]:2000, pt[0][0]:2000]
cv2.imwrite('cropfinal.jpg',crop_img)