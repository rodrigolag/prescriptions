with open('..\Output\kanji.txt', 'r') as f:
    lines = f.readlines()

lines = [line.replace('\n', '') for line in lines]

with open('..\Output\kanji.txt', 'w') as f:
    f.writelines(lines)