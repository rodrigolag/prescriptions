# -*- coding: utf-8 -*-
"""
2018/04/06 - bssb
"""
import sys
sys.path.append(r'C:\Users\roris\Documents\mirai\USB_Backup\Python27\Lib\site-packages')
import wx

from simple_base import TwainBase
from subprocess import check_output

import traceback, sys

ID_EXIT=102
ID_OPEN_SCANNER=103
ID_ACQUIRE_NATIVELY=104
ID_SCROLLEDWINDOW1=105
ID_BMPIMAGE=106
ID_ACQUIRE_BY_FILE=107
ID_TIMER=108

# You can either Poll the TWAIN source, or process the scanned image in an
# event callback. The event callback has not been fully tested using GTK.
# Specifically this does not work with Tkinter.
USE_CALLBACK=True


class MainFrame(wx.Frame, TwainBase):
    """wxPython implementation of the simple demonstration"""
    def __init__(self, parent, id, title):
        wx.Frame.__init__(self, parent, id, title,
        wx.DefaultPosition, wx.Size(800,600))

        font0 = wx.Font(22, wx.DECORATIVE, wx.ITALIC, wx.NORMAL)
        font1 = wx.Font(18, wx.DECORATIVE, wx.ITALIC, wx.NORMAL)
        font2 = wx.Font(12, wx.DECORATIVE, wx.ITALIC, wx.NORMAL)

        self.Maximize(True)
        self.CreateStatusBar()
        menu = wx.Menu()
        menu.Append(ID_OPEN_SCANNER, "ソースの選択", "スキャナに接続する")
        menu.Append(ID_ACQUIRE_NATIVELY, "スキャン", "ネ処方箋画像を取得する")
        #menu.Append(ID_ACQUIRE_BY_FILE, "Acquire By &File", "Acquire an Image using File Transfer Interface")
        menu.AppendSeparator()
        menu.Append(ID_EXIT, "閉じる", "プログラムを終了する")
        menuBar = wx.MenuBar()
        menuBar.Append(menu, "ファイル")
        self.SetMenuBar(menuBar)

        wx.EVT_MENU(self, ID_EXIT, self.MnuQuit)
        wx.EVT_MENU(self, ID_OPEN_SCANNER, self.MnuOpenScanner)
        wx.EVT_MENU(self, ID_ACQUIRE_NATIVELY, self.MnuAcquireNatively)
        wx.EVT_MENU(self, ID_ACQUIRE_BY_FILE, self.MnuAcquireByFile)
        wx.EVT_CLOSE(self, self.OnClose)

        #Panel positioning for all information
        panel = wx.Panel(self, -1)

        #Prescription image label
        picture_label = wx.StaticText(panel, pos=(20, 10), size=(100, 100), label="処方箋画像")
        picture_label.SetFont(font0)

        self.scrolledWindow1 = wx.ScrolledWindow(panel,wx.ID_ANY,pos=(60, 50), size=(600, 600))
        self.bmpImage = wx.StaticBitmap(bitmap = wx.NullBitmap, id = ID_BMPIMAGE, name = 'bmpImage', parent = self.scrolledWindow1, pos = wx.Point(0,0), style = 0)


        #Patient info label
        patient_label = wx.StaticText(panel, pos=(700, 10), size=(250, 50), label="患者情報")
        patient_label.SetFont(font0)

        #Source selection button
        self.btn_source = wx.Button(panel, label="ソース選択",pos=(145, 660), size=(90, 90))
        self.btn_source.SetFont(font2)
        self.btn_source.Bind(wx.EVT_BUTTON,self.MnuOpenScanner)

        #Scan prescription button
        self.btn_scan = wx.Button(panel, label="処方箋\nスキャン",pos=(295, 660), size=(90, 90))
        self.btn_scan.SetFont(font2)
        self.btn_scan.Bind(wx.EVT_BUTTON,self.MnuAcquireNatively)

        #Scan prescription button
        self.btn_fill = wx.Button(panel, label="データ取得",pos=(445, 660), size=(90, 90))
        self.btn_fill.SetFont(font2)
        self.btn_fill.Bind(wx.EVT_BUTTON,self.MnuFill)

        #Automatic fill button
        self.btn_fill = wx.Button(panel, label="自動Do入力",pos=(1100, 700), size=(100, 40))
        self.btn_fill.SetFont(font2)
        self.btn_fill.Bind(wx.EVT_BUTTON,self.MnuFill)

        #Patient
        patient_label = wx.StaticText(panel, pos=(700, 90), size=(30, 50), label="患\n者")
        patient_label.SetFont(font0)

        patient_label1 = wx.StaticText(panel, pos=(750, 110), size=(40, 35), label="氏名")
        patient_label1.SetFont(font1)

        txt_kana = wx.TextCtrl(panel, -1, 'かな', pos=(820, 80), size=(200, 35))
        txt_kana.SetFont(font1)

        txt_kanji = wx.TextCtrl(panel, -1, '漢字', pos=(820, 120), size=(200, 35))
        txt_kanji.SetFont(font1)

        patient_label2 = wx.StaticText(panel, pos=(1070, 85), size=(50, 35), label="保険保険者番号")
        patient_label2.SetFont(font1)

        txt_insurance = wx.TextCtrl(panel, -1, '番号', pos=(1275, 80), size=(200, 35))
        txt_insurance.SetFont(font1)

        patient_label3 = wx.StaticText(panel, pos=(1030, 140), size=(50, 35), label="公費負担者番号(上)")
        patient_label3.SetFont(font1)

        txt_kohi1 = wx.TextCtrl(panel, -1, '番号', pos=(1275, 135), size=(200, 35))
        txt_kohi1.SetFont(font1)

        patient_label4 = wx.StaticText(panel, pos=(1090, 185), size=(50, 35), label="公費負担医療\nの受給者番号")
        patient_label4.SetFont(font1)

        txt_kohi2 = wx.TextCtrl(panel, -1, '番号', pos=(1275, 195), size=(200, 35))
        txt_kohi2.SetFont(font1)

        patient_label5 = wx.StaticText(panel, pos=(1030, 250), size=(50, 35), label="公費負担者番号(下)")
        patient_label5.SetFont(font1)

        txt_kohi3 = wx.TextCtrl(panel, -1, '番号', pos=(1275, 245), size=(200, 35))
        txt_kohi3.SetFont(font1)

        patient_label6 = wx.StaticText(panel, pos=(700, 180), size=(50, 35), label="生年月日")
        patient_label6.SetFont(font1)

        txt_birth_date = wx.TextCtrl(panel, -1, '生年月日', pos=(820, 180), size=(200, 35))
        txt_birth_date.SetFont(font1)

        patient_label7 = wx.StaticText(panel, pos=(700, 250), size=(50, 35), label="性別")
        patient_label7.SetFont(font1)

        txt_sex = wx.TextCtrl(panel, -1, '性', pos=(770, 245), size=(50, 35))
        txt_sex.SetFont(font1)

        patient_label8 = wx.StaticText(panel, pos=(850, 250), size=(50, 35), label="区分")
        patient_label8.SetFont(font1)

        txt_family = wx.TextCtrl(panel, -1, '区分', pos=(910, 245), size=(110, 35))
        txt_family.SetFont(font1)

        patient_label9 = wx.StaticText(panel, pos=(1125, 40), size=(50, 35), label="保険医氏名")
        patient_label9.SetFont(font1)

        txt_doctor_name = wx.TextCtrl(panel, -1, '氏名', pos=(1275, 35), size=(200, 35))
        txt_doctor_name.SetFont(font1)

        # Print out the exception - requires that you run from the command prompt
        sys.excepthook = traceback.print_exception

        # Initialise the Twain Base Class
        self.Initialise()

        # Polling based example
        if not USE_CALLBACK:
            wx.EVT_TIMER(self, ID_TIMER, self.onIdleTimer)
            self.timer=wx.Timer(self, ID_TIMER)
            self.timer.Start(250)

    def MnuQuit(self, event):
        self.Close(1)

    def OnClose(self, event):
        # Terminate the Twain Base Class
        self.Terminate()
        self.Destroy()

    def MnuOpenScanner(self, event):
        self.OpenScanner(self.GetHandle(), ProductName="処方箋マスター", UseCallback=USE_CALLBACK)

    def MnuAcquireNatively(self, event):
        return self.AcquireNatively()

    def MnuAcquireByFile(self, event):
        return self.AcquireByFile()

    #Automatic Field Filler
    def MnuFill(self, event):
        check_output("python ..\Scripts\img_cropper.py tmp.bmp", shell=True)
        check_output("python ..\OCREngine\print_result.py", shell=True)

    def DisplayImage(self, ImageFileName):
        bmp = wx.Image(ImageFileName, wx.BITMAP_TYPE_BMP).ConvertToBitmap()
        image = wx.ImageFromBitmap(bmp)
        image = image.Scale(10, 10, wx.IMAGE_QUALITY_HIGH)
        self.bmpImage.SetBitmap(bmp)
        self.scrolledWindow1.maxWidth = bmp.GetWidth()
        self.scrolledWindow1.maxHeight = bmp.GetHeight()
        self.scrolledWindow1.SetScrollbars(40, 40, bmp.GetWidth()/40, bmp.GetHeight()/40)
        self.bmpImage.Refresh()

    def LogMessage(self, message):
        # Set the title on the main window - used for tracing
        self.SetTitle(message)

    def onIdleTimer(self, event=None):
        """This is a polling mechanism. Get the image without relying on the callback."""
        self.PollForImage()

class SimpleApp(wx.App):
    def OnInit(self):
        frame = MainFrame(None, -1, "処方箋マスター　V.0.1")
        frame.Show(True)
        self.SetTopWindow(frame)
        return 1

SimpleApp(0).MainLoop()

#Commenting out information for template matching and selection
