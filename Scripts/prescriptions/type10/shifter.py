import cv2
import numpy as np
import sys

img_rgb = cv2.imread('deskew_imgs/1.jpg')
img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
template = cv2.imread('reference.jpg', 0)
w, h = template.shape[::-1]
totalshift = 0

res = cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)
threshold = 0.6
loc = np.where( res >= threshold)
for pt in zip(*loc[::-1]):
    a=pt[0]
    b=pt[1]
if a > 200:
    shift = 0
    while a > 200:
        shift = shift + 50
        totalshift = totalshift + shift
        a = a - shift

#using translation matrix we shift the image to the left by totalshift pixels
if totalshift > 0:
    M = np.float32([[1,0,-totalshift], [0,1,0]])
    shifted = cv2.warpAffine(img_rgb,M,(img_rgb.shape[1],img_rgb.shape[0])) #shifted images
    cropfinal = shifted[b:3000, a:2000]
    cv2.imwrite('shiftTest.jpg', cropfinal)
else:
    cropfinal = img_rgb[b:3000, a:2000]
    cv2.imwrite('shiftTest.jpg', cropfinal)
