type_2 = {"kana": {'x':200, 'y':190, 'w':240, 'h':45 },
          "kanji": {'x':200, 'y':240, 'w':340, 'h':45 },
          "sex": {'x':620, 'y':300, 'w':45, 'h':45},
          "birth_date": {'x':200, 'y':300, 'w':395, 'h':35 },
          "family": {'x':260, 'y':350, 'w':270, 'h':30 },
          "insurance": {'x':1070, 'y':25, 'w':470, 'h':40 },
          "kohi1" : {'x':210, 'y':25, 'w':430, 'h':40},
          "kohi2" : {'x':210, 'y':110, 'w':390, 'h':40},
          "kohi3" : {'x':1000, 'y':1945, 'w':550, 'h':45},
          "doctor" : {'x':980, 'y':310, 'w':260, 'h':50},
          "yakuhin" : {'ln':18,'x':220,'y':635,'lh':49, 'yakuhin_w':870,'ryo_w':170,'kosai_w':130,'kikan_w':140}
        }
dict = [type_2]
