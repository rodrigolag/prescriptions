import numpy as np
import cv2


def nearby(a1, a2, error):
    cases = np.unwrap([a2-error, a1, a2 + error])
    return cases[0] <= cases[1] <= cases[2]

def deskew(input, output):
    # read image
    img = cv2.imread(input)

    # grayscale it and detect borderes
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    binary = cv2.Canny(gray,50,150,apertureSize = 3)

    # use Hough transform to find lines in the binary image
    # with a resolution of half angle (pi/720) and keeping
    # those lines that reach a score of 1000 or more
    # (the longest ones)
    lines = cv2.HoughLines(binary, 2, np.pi/720, 1000)

    # get the angles found by the Hough transform
    # for each of the found lines
    angles = []
    for line in lines:
        rho, theta = line[0]
        if rho<0:
            theta = -theta

        # stay with those lines next to being horizontal
        # (with an erro of +- 10 degrees)
        if not nearby(theta, np.pi/2, np.deg2rad(10)):
           continue;

        angles.append(theta)

    # count how many times the angle appears
    from collections import Counter
    count = Counter(angles)

    # keep with the three more frequent cases
    frequent = count.most_common(3)

    # average them
    suma = sum(angle*repeat for angle,repeat in frequent)
    repeat = sum(repeat for angle, repeat in frequent)
    angle = suma/repeat

    angle = np.rad2deg(angle - np.pi/2)
    print("[INFO] angle: %.5f" % angle)

    # rotate the image with the given angle
    (h, w) = img.shape[:2]
    center = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D(center, angle, 1.0)

    rotate = cv2.warpAffine(img, M, (w, h),
                flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)

    cv2.imwrite(output, rotate)

#deskew('IMG.jpg', 'skewed.jpg')
