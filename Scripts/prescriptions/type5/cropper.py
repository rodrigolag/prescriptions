import cv2
import numpy as np
import sys
from PIL import Image


#cv2.imwrite('res.jpg',img_rgb)

#Loading new cropped template
#res = cv2.imread('res.jpg')

#cv2.imwrite('cropped.jpg',cropfinal)

def cropper(input_img, output_name):
        img_rgb = cv2.imread(input_img)
        img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
        template = cv2.imread('reference.jpg',0)
        w, h = template.shape[::-1]
        totalshift = 0

        #TM_CCOEFF_NORMED
        res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
        threshold = 0.65
        loc = np.where( res >= threshold)

        for pt in zip(*loc[::-1]):
            a=pt[0]
            b=pt[1]
        if a > 200:
            shift = 0
            while a > 200:
                shift = shift + 50
                totalshift = totalshift + shift
                a = a - shift

        #using translation matrix we shift the image to the left by totalshift pixels
        if totalshift > 0:
            M = np.float32([[1,0,-totalshift], [0,1,0]])
            shifted = cv2.warpAffine(img_rgb,M,(img_rgb.shape[1],img_rgb.shape[0])) #shifted images
            cropfinal = shifted[b:3000, a:2000]
            cv2.imwrite(output_name, cropfinal)
        else:
            cropfinal = img_rgb[b:3000, a:2000]
            cv2.imwrite(output_name, cropfinal)

#cropper('deskew_imgs/1.jpg', 'crop1.jpg')
