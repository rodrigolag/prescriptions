import numpy as np
import cv2

cap = cv2.VideoCapture('OPENcv_TWAIN_Color2_conluz.jpg')
scale_factor=1.5
if cap.isOpened():
    # Capture frame-by-frame
    ret, frame = cap.read()
    (h, w) = frame.shape[:2]
    # calculate the center of the image
    center = (w / 2, h / 2)

    angle90 = 90
    angle180 = 180
    angle270 = 270

    scale = 1.0

    # Perform the counter clockwise rotation holding at the center
    # 90 degrees
    M = cv2.getRotationMatrix2D(center, angle90, scale)
    sframe = cv2.warpAffine(frame, M, (0, 0))

    #frame = cv2.resize(frame, (0, 0), fx=scale_factor, fy=scale_factor)
    # Our operations on the frame come here
    gray = cv2.cvtColor(sframe, cv2.COLOR_BGR2GRAY)
    #gray = frame

    #his1 = cv2.equalizeHist(gray)
    #clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    adapta = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 121, 1)
    #cl1 = clahe.apply(gray)
    #
    # Display the resulting frame
    cv2.imshow("Color",frame)
    #cv2.imshow("OPENcv_TWAIN_gray",gray)
    #cv2.imshow("OPENcv_TWAIN_clahe", cl1)
    #cv2.imshow("OPENcv_TWAIN_histogram", his1)
    cv2.imshow("OPENcv_TWAIN_ADAPTATIVE", adapta)
    cv2.waitKey(0)
#    if cv2.waitKey(1) & 0xFF == ord('q'):
#        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
#cv2.imwrite("OPENcv_TWAIN_Color.jpg", frame)
#cv2.imwrite("OPENcv_TWAIN_Gray.jpg", gray)
#cv2.imwrite("OPENcv_TWAIN_Clahe.jpg", cl1)
#cv2.imwrite("OPENcv_TWAIN_Histogram.jpg", his1)
cv2.imwrite("OPENcv_TWAIN_Adaptative2.jpg", adapta)
