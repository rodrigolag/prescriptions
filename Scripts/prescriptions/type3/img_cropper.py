# -*- coding: utf-8 -*-
"""
2018/04/06 - bssb
"""
import sys
import cv2
import numpy as np
from cropper import *
from template_info import *
from hough_transform import *
from PIL import Image

#大今里ふれあいクリニック (x, y, width, height)
"""
imgs = ['source_imgs/IMG.jpg', 'source_imgs/IMG_0001.jpg', 'source_imgs/IMG_0002.jpg', 'source_imgs/IMG_0003.jpg', 'source_imgs/IMG_0004.jpg']
c = 0

#Deskew the images first
for image in imgs:
	deskew(image, 'deskew_imgs/' + str(c) + '.jpg')
	c = c + 1

imgs = ['deskew_imgs/0.jpg', 'deskew_imgs/1.jpg', 'deskew_imgs/2.jpg', 'deskew_imgs/3.jpg', 'deskew_imgs/4.jpg']

c = 0
#By match template crop them to a standard size
for image in imgs:
	cropper(image, 'crops/crop' + str(c) + '.jpg');
	c = c + 1
"""


template = dict[0]

kana = template.get("kana", "")
kanji = template.get("kanji", "")
birth_date = template.get("birth_date", "")
sex_male = template.get("sex_male", "")
sex_female = template.get("sex_female", "")
family_one = template.get("family_one", "")
family_many = template.get("family_many", "")
insurance = template.get("insurance", "")
kohi1 = template.get("kohi1", "")
kohi2 = template.get("kohi2", "")
kohi3= template.get("kohi3", "")
doctor_name  = template.get("doctor", "")
yakuhin = template.get("yakuhin","")

#Yakuhin lines array (empty)
yakuhin_line=[]



#Loading template images
shohosen_img = cv2.imread('crops/crop0.jpg')

#print("../Samples/" + str(sys.argv[1]))
#print(str(sys.argv[1]))
kana_img = shohosen_img[kana['y']:kana['y']+kana['h'], kana['x']:kana['x']+kana['w']]
kanji_img = shohosen_img[kanji['y']:kanji['y']+kanji['h'], kanji['x']:kanji['x']+kanji['w']]
birth_date_img = shohosen_img[birth_date['y']:birth_date['y']+birth_date['h'], birth_date['x']:birth_date['x']+birth_date['w']]
sex_male_img = shohosen_img[sex_male['y']:sex_male['y']+sex_male['h'], sex_male['x']:sex_male['x']+sex_male['w']]
sex_female_img = shohosen_img[sex_female['y']:sex_female['y']+sex_female['h'], sex_female['x']:sex_female['x']+sex_female['w']]
family_one_img = shohosen_img[family_one['y']:family_one['y']+family_one['h'], family_one['x']:family_one['x']+family_one['w']]
family_many_img = shohosen_img[family_many['y']:family_many['y']+family_many['h'], family_many['x']:family_many['x']+family_many['w']]
insurance_img = shohosen_img[insurance['y']:insurance['y']+insurance['h'], insurance['x']:insurance['x']+insurance['w']]
kohi1_img = shohosen_img[kohi1['y']:kohi1['y']+kohi1['h'], kohi1['x']:kohi1['x']+kohi1['w']]
kohi2_img = shohosen_img[kohi2['y']:kohi2['y']+kohi2['h'], kohi2['x']:kohi2['x']+kohi2['w']]
kohi3_img = shohosen_img[kohi3['y']:kohi3['y']+kohi3['h'], kohi3['x']:kohi3['x']+kohi3['w']]
doctor_name_img = shohosen_img[doctor_name['y']:doctor_name['y']+doctor_name['h'], doctor_name['x']:doctor_name['x']+doctor_name['w']]

cv2.imwrite('crop_parts/kana.jpg',kana_img)
cv2.imwrite('crop_parts/kanji.jpg',kanji_img)
cv2.imwrite('crop_parts/sex_male.jpg',sex_male_img)
cv2.imwrite('crop_parts/sex_femal.jpg',sex_female_img)
cv2.imwrite('crop_parts/birth_date.jpg',birth_date_img)
cv2.imwrite('crop_parts/family_one.jpg',family_one_img)
cv2.imwrite('crop_parts/family_many.jpg',family_many_img)
cv2.imwrite('crop_parts/insurance.jpg',insurance_img)

cv2.imwrite('crop_parts/kohi1.jpg',kohi1_img)
cv2.imwrite('crop_parts/kohi2.jpg',kohi2_img)
cv2.imwrite('crop_parts/kohi3.jpg',kohi3_img)
cv2.imwrite('crop_parts/doctor_name.jpg',doctor_name_img)



for i in range(yakuhin['ln']):
	yakuhin_name_img = shohosen_img[yakuhin['y']+yakuhin['lh']*i+4:yakuhin['y']+yakuhin['lh']*(1+i)+i, yakuhin['x']:yakuhin['x']+yakuhin['yakuhin_w']]
	ryo_name_img = shohosen_img[yakuhin['y']+yakuhin['lh']*i+4:yakuhin['y']+yakuhin['lh']*(1+i)+i, yakuhin['x']+yakuhin['yakuhin_w']:yakuhin['x']+yakuhin['yakuhin_w']+yakuhin['ryo_w']]
	kosai_name_img = shohosen_img[yakuhin['y']+yakuhin['lh']*i+4:yakuhin['y']+yakuhin['lh']*(1+i)+i, yakuhin['x']+yakuhin['yakuhin_w']+yakuhin['ryo_w']:yakuhin['x']+yakuhin['yakuhin_w']+yakuhin['ryo_w']+yakuhin['kosai_w']]
	kikan_name_img = shohosen_img[yakuhin['y']+yakuhin['lh']*i+4:yakuhin['y']+yakuhin['lh']*(1+i)+i, yakuhin['x']+yakuhin['yakuhin_w']+yakuhin['ryo_w']+yakuhin['kosai_w']:yakuhin['x']+yakuhin['yakuhin_w']+yakuhin['ryo_w']+yakuhin['kosai_w']+yakuhin['kikan_w']]
	cv2.imwrite('crop_parts/yakuhin'+str(i)+'.jpg',yakuhin_name_img)
	cv2.imwrite('crop_parts/ryo'+str(i)+'.jpg',ryo_name_img)
	cv2.imwrite('crop_parts/kosai'+str(i)+'.jpg',kosai_name_img)
	cv2.imwrite('crop_parts/kikan'+str(i)+'.jpg',kikan_name_img)

"""
image_names = ['crop_parts/kana.jpg','crop_parts/kanji.jpg','crop_parts/sex.jpg','crop_parts/birth_date.jpg','crop_parts/family.jpg','crop_parts/insurance.jpg','crop_parts/kohi1.jpg','crop_parts/kohi2.jpg','crop_parts/kohi3.jpg', 'crop_parts/doctor_name.jpg']
images = []
max_width = 0 # find the max width of all the images
total_height = 0 # the total height of the images (vertical stacking)

for name in image_names:
    # open all images and find their sizes
    images.append(cv2.imread(name))
    if images[-1].shape[1] > max_width:
        max_width = images[-1].shape[1]
    total_height += images[-1].shape[0]

# create a new array with a size large enough to contain all the images
final_image = np.zeros((total_height,max_width,3),dtype=np.uint8)

current_y = 0 # keep track of where your current image was last placed in the y coordinate
for image in images:
    # add an image to the final array and increment the y coordinate
    final_image[current_y:image.shape[0]+current_y,:image.shape[1],:] = image
    current_y += image.shape[0]

cv2.imwrite('crop.jpg',final_image)
"""
