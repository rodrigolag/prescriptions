# -*- coding: utf-8 -*-
"""
2018/04/06 - bssb
"""
import cv2
import numpy as np
from template_info import *
import sys
from PIL import Image
 
#大今里ふれあいクリニック (x, y, width, height)

template = dict[0]

kana = template.get("kana", "")
kanji = template.get("kanji", "")
birth_date = template.get("birth_date", "")
sex = template.get("sex", "")
family = template.get("family", "")
insurance = template.get("insurance", "")
kohi1 = template.get("kohi1", "")
kohi2 = template.get("kohi2", "")
kohi3= template.get("kohi3", "")
doctor_name  = template.get("doctor", "")
yakuhin = template.get("yakuhin","")

#Yakuhin lines array (empty)
yakuhin_line=[]

#Rotate scanned file
img=Image.open(str(sys.argv[1]))
img=img.rotate(90,expand=True)
img.save("tmp.bmp")

#Loading template images
shohosen_img = cv2.imread(str(sys.argv[1]))
print("../Samples/" + str(sys.argv[1]))
kana_img = shohosen_img[kana['y']:kana['y']+kana['h'], kana['x']:kana['x']+kana['w']]
kanji_img = shohosen_img[kanji['y']:kanji['y']+kanji['h'], kanji['x']:kanji['x']+kanji['w']]
sex_img = shohosen_img[birth_date['y']:birth_date['y']+birth_date['h'], birth_date['x']:birth_date['x']+birth_date['w']]
birth_date_img = shohosen_img[sex['y']:sex['y']+sex['h'], sex['x']:sex['x']+sex['w']]
family_img = shohosen_img[family['y']:family['y']+family['h'], family['x']:family['x']+family['w']]
insurance_img = shohosen_img[insurance['y']:insurance['y']+insurance['h'], insurance['x']:insurance['x']+insurance['w']]
kohi1_img = shohosen_img[kohi1['y']:kohi1['y']+kohi1['h'], kohi1['x']:kohi1['x']+kohi1['w']]
kohi2_img = shohosen_img[kohi2['y']:kohi2['y']+kohi2['h'], kohi2['x']:kohi2['x']+kohi2['w']]
kohi3_img = shohosen_img[kohi3['y']:kohi3['y']+kohi3['h'], kohi3['x']:kohi3['x']+kohi3['w']]
doctor_name_img = shohosen_img[doctor_name['y']:doctor_name['y']+doctor_name['h'], doctor_name['x']:doctor_name['x']+doctor_name['w']]

cv2.imwrite('../Python27/Crops/kana.jpg',kana_img)
cv2.imwrite('../Python27/Crops/kanji.jpg',kanji_img)
cv2.imwrite('../Python27/Crops/sex.jpg',sex_img)
cv2.imwrite('../Python27/Crops/birth_date.jpg',birth_date_img)
cv2.imwrite('../Python27/Crops/family.jpg',family_img)
cv2.imwrite('../Python27/Crops/insurance.jpg',insurance_img)

cv2.imwrite('../Python27/Crops/kohi1.jpg',kohi1_img)
cv2.imwrite('../Python27/Crops/kohi2.jpg',kohi2_img)
cv2.imwrite('../Python27/Crops/kohi3.jpg',kohi3_img)
cv2.imwrite('../Python27/Crops/doctor_name.jpg',doctor_name_img)

for i in range(yakuhin['ln']):
	yakuhin_name_img = shohosen_img[yakuhin['y']+yakuhin['lh']*i+4:yakuhin['y']+yakuhin['lh']*(1+i)+i, yakuhin['x']:yakuhin['x']+yakuhin['yakuhin_w']]
	ryo_name_img = shohosen_img[yakuhin['y']+yakuhin['lh']*i+4:yakuhin['y']+yakuhin['lh']*(1+i)+i, yakuhin['x']+yakuhin['yakuhin_w']:yakuhin['x']+yakuhin['yakuhin_w']+yakuhin['ryo_w']]
	kosai_name_img = shohosen_img[yakuhin['y']+yakuhin['lh']*i+4:yakuhin['y']+yakuhin['lh']*(1+i)+i, yakuhin['x']+yakuhin['yakuhin_w']+yakuhin['ryo_w']:yakuhin['x']+yakuhin['yakuhin_w']+yakuhin['ryo_w']+yakuhin['kosai_w']]
	kikan_name_img = shohosen_img[yakuhin['y']+yakuhin['lh']*i+4:yakuhin['y']+yakuhin['lh']*(1+i)+i, yakuhin['x']+yakuhin['yakuhin_w']+yakuhin['ryo_w']+yakuhin['kosai_w']:yakuhin['x']+yakuhin['yakuhin_w']+yakuhin['ryo_w']+yakuhin['kosai_w']+yakuhin['kikan_w']]
	cv2.imwrite('../Python27/Crops/yakuhin'+str(i)+'.jpg',yakuhin_name_img)
	cv2.imwrite('../Python27/Crops/ryo'+str(i)+'.jpg',ryo_name_img)
	cv2.imwrite('../Python27/Crops/kosai'+str(i)+'.jpg',kosai_name_img)
	cv2.imwrite('../Python27/Crops/kikan'+str(i)+'.jpg',kikan_name_img)


##image_names = ['.../OCREngine/Crops/kana.jpg','../OCREngine/Crops/kanji.jpg','../OCREngine/Crops/sex.jpg','../OCREngine/Crops/birth_date.jpg','../OCREngine/Crops/family.jpg','../OCREngine/Crops/insurance.jpg','../OCREngine/Crops/kohi1.jpg','../OCREngine/Crops/kohi2.jpg','.../OCREngine/Crops/kohi3.jpg', '../OCREngine/Crops/doctor_name.jpg']
##images = []
##max_width = 0 # find the max width of all the images
##total_height = 0 # the total height of the images (vertical stacking)
##
##for name in image_names:
##    # open all images and find their sizes
##    images.append(cv2.imread(name))
##    if images[-1].shape[1] > max_width:
##        max_width = images[-1].shape[1]
##    total_height += images[-1].shape[0]
##
### create a new array with a size large enough to contain all the images
##final_image = np.zeros((total_height,max_width,3),dtype=np.uint8)
##
##current_y = 0 # keep track of where your current image was last placed in the y coordinate
##for image in images:
##    # add an image to the final array and increment the y coordinate
##    final_image[current_y:image.shape[0]+current_y,:image.shape[1],:] = image
##    current_y += image.shape[0]
##
##cv2.imwrite('crop.jpg',final_image)


